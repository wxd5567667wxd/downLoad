package com.down.utls;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import com.down.main.StaticInfo;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.keeley.core.DownFileFetch;
import com.keeley.core.DownFileInfoBean;
import com.keeley.listen.DownListener;

public class ThreadPool {

	public static Map<Integer, DownFileFetch> jobs = new HashMap<Integer, DownFileFetch>();

	public static void addthread(DownFileFetch e, int idme, Shell s) {
		System.out.println("addthread" + jobs.size());
		if (jobs.size() > 5) {
			MessageDialog.openInformation(s, "超过最大下载数量", "已经超过最大下载数量,已经保存下载信息但是不能马上开始下载!");
			return;
		}
		jobs.put(idme, e);
		e.start();
	}

	public static void stopall() {
		System.out.println("stopall" + jobs.size());

	}

	public static void stopbyid(Integer idme, Shell s) {
		System.out.println("stopbyid" + idme + "+,size:" + jobs.size());
		DownFileFetch k = jobs.get(idme);
		if (null == k) {//这里是处理垃圾数据的，正式环境应该不会发生
			deletethread(idme, StaticInfo.stop);
			MessageDialog.openInformation(s, "下载任务", "下载任务已经暂停!");
			return;
		}
		if (null != k && k.isAlive()) {
			k.siteStop();
			deletethread(idme, StaticInfo.stop);
			MessageDialog.openInformation(s, "下载任务", "下载任务已经暂停!");
			return;
		} else {
			MessageDialog.openInformation(s, "下载任务", "下载任务已经完成!");
			return;
		}
	}

	public static void startbyid(final Integer idme, Shell s) {
		System.out.println("startbyid" + idme + "+,size:" + jobs.size());
		Record e = Db.findFirst("select * from down where id=?;", idme);
		if (null != e) {
			Record r = Db.findFirst("select * from config where name='mulu'");
			DownFileInfoBean bean = new DownFileInfoBean(e.getStr("url"), r.getStr("value"), e.getStr("filename"), 5,
					true, null);

			DownFileFetch fileFetch = null;
			try {
				// downidk = downid;
				fileFetch = new DownFileFetch(bean, idme);
				fileFetch.addListener(new DownListener() {
					public void success() {
						System.out.println("下载成功!来自ThreadPool 70行 回调监控");

						deletethread(idme, StaticInfo.ok);

					}
				});
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			addthread(fileFetch, idme, s);
			MessageDialog.openInformation(s, "下载任务", "下载任务已经开始继续断点续传下载!");
			return;
		}
	}

	public static void deletethread(Integer i, String status) {

		System.out.println("deletethread" + jobs.size() + "id:" + i);
		jobs.remove(i);
		System.out.println("deletethread" + jobs.size());
		Db.update("update down set downstatus=? where id=?;", status, i);

	}

	public static void close() {
		// TODO Auto-generated method stub
		Iterator<Integer> keys = jobs.keySet().iterator();
		while (keys.hasNext()) {
			Integer key = (Integer) keys.next();
			DownFileFetch value = jobs.get(key);
			value.siteStop();
		}
	}

}
